#include "Tools.h"



Tools::Tools()
{
}

cv::Mat Tools::GetImage(std::string fname)
{
	try
	{
		return cv::imread(fname);
	}
	catch (...)
	{
		return cv::Mat();
	}
}

cv::Mat Tools::KuwaharaGray(cv::Mat& img, int kernel_size)
{
	int t = img.type();
	int nchannels = img.channels();
	int w = img.cols;
	int h = img.rows;
	cv::Mat out(h, w, t);//= cvCreateImage(cvSize(w,h),IPL_DEPTH_8U,nchannels);

	int dt = kernel_size / 2;

	//TODO: ��������� �����������-������ ������ ����.

	if (nchannels == 1)  //������ ��� �������������
	{
		for (int y = 2 * dt; y < h - 2 * dt; ++y)
		{
			uchar* ptr = (uchar*)(img.data + y*img.step);//->widthStep);
			uchar* p_out = (uchar*)(out.data + y*out.step);
			for (int x = 2 * dt; x < w - 2 * dt; ++x)
			{
				int gr = 0;
				int sum = 0;
				double mean1 = 0;
				double mean2 = 0;
				double mean3 = 0;
				double mean4 = 0;
				double disp1 = 0;
				double disp2 = 0;
				double disp3 = 0;
				double disp4 = 0;

				int k = 0;
				//1
				sum = 0;
				mean1 = 0;
				uchar* pd = ptr;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						sum += pd[x + k2];
						++k;
					}
					pd += img.step;
				}
				mean1 = sum / ((dt + 1)*(dt + 1));

				pd = ptr;
				disp1 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						disp1 += (pd[x + k2] - mean1)*(pd[x + k2] - mean1);
					}
					pd += img.step;
				}

				//2
				sum = 0;
				mean2 = 0;
				pd = ptr + dt*img.step;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						sum += pd[x + k2];
					}
					pd += img.step;
				}
				mean2 = sum / ((dt + 1)*(dt + 1));

				pd = ptr + dt*img.step;
				disp2 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = -dt; k2 <= 0; ++k2)
					{
						disp2 += (pd[x + k2] - mean2)*(pd[x + k2] - mean2);
					}
					pd += img.step;
				}

				//3
				sum = 0;
				mean3 = 0;
				pd = ptr;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						sum += pd[x + k2];
					}
					pd += img.step;
				}
				mean3 = sum / ((dt + 1)*(dt + 1));

				pd = ptr;
				disp3 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						disp3 += (pd[x + k2] - mean3)*(pd[x + k2] - mean3);
					}
					pd += img.step;
				}

				//4
				sum = 0;
				mean4 = 0;
				pd = ptr + dt*img.step;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						sum += pd[x + k2];
					}
					pd += img.step;
				}
				mean4 = sum / ((dt + 1)*(dt + 1));

				pd = ptr + dt*img.step;
				disp4 = 0;
				for (int k1 = 0; k1 <= dt; ++k1)
				{
					for (int k2 = 0; k2 <= dt; ++k2)
					{
						disp4 += (pd[x + k2] - mean4)*(pd[x + k2] - mean4);
					}
					pd += img.step;
				}

				gr = 0;
				if ((disp1 <= disp2) && (disp1 <= disp3) && (disp1 <= disp4))
					gr = mean1;
				if ((disp2 <= disp1) && (disp2 <= disp3) && (disp2 <= disp4))
					gr = mean2;
				if ((disp3 <= disp1) && (disp3 <= disp2) && (disp3 <= disp4))
					gr = mean3;
				if ((disp4 <= disp1) && (disp4 <= disp2) && (disp4 <= disp3))
					gr = mean4;

				p_out[x] = gr;
			}
		}
	}
	return out;
}


Tools::~Tools()
{
}
