#pragma once

#include <vector>

#include "opencv\cv.hpp"
#include "opencv2\core.hpp"
#include "opencv\cv.hpp"
#include "opencv\cv.h"
#include "opencv\highgui.h"
#include "Resizer.h"
#include "Tools.h"

#define DOWN_TO_ROWS 256
#define DOWN_TO_COLS 380
#define KUWAHARA_WIN 7
#define GAUSS_KERNEL 3

class Segmenter
{
public:
	Segmenter();
	int SegmentImg(cv::Mat& input, cv::Mat& output);
	int ApplyMap(cv::Mat& map, cv::Mat& input, cv::Mat& sky, cv::Mat& ground);
	virtual ~Segmenter();
};

