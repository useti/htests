#pragma once

#include "opencv\cv.hpp"
#include "opencv\highgui.h"
#include "opencv2\imgproc.hpp"
//#include "boost\date_time.hpp"

#ifdef PI
#undef PI                /* PI is defined in Linux */
#endif
#define PI   3.14159265358979
#define TPI  6.28318530717959     /* PI*2 */

#define INIT_TIMECHECK() \
	double timecheck = (double)cv::getTickCount();

#define START_TIMECHECK() timecheck = (double)cv::getTickCount();

#define STOP_TIMECHECK() \
	timecheck = ((double)cv::getTickCount() - timecheck)/cv::getTickFrequency();\
	std::cout << "Times passed in seconds: " << timecheck << std::endl;


class Tools
{
public:
	Tools();

	static cv::Mat GetImage(std::string fname);
	static cv::Mat KuwaharaGray(cv::Mat& img, int kernel_size);

	~Tools();
};

