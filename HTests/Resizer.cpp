#include "Resizer.h"



Resizer::Resizer()
{
}

//************************************
// Method:    ResizeImg
// FullName:  Resizer::ResizeImg
// Access:    public 
// Returns:   int
// Qualifier: Entry point
// Parameter: cv::Mat & input
// Parameter: cv::Mat & output
// Parameter: double scale
// Parameter: unsigned int algorythm
// Parameter: unsigned int w_sz
//************************************
int Resizer::ResizeImg(
	cv::Mat& input, 
	cv::Mat& output, 
	double scale /*= 1*/, 
	unsigned int algorythm /*= RSZ_NEAREST*/, 
	unsigned int w_sz /*= 4*/
	)
{
	switch (algorythm) {
	case RSZ_NEAREST:
		ResizeNearest(input, output, scale);
		break;
	case RSZ_BILINER:
		ResizeBilinear(input, output, scale, w_sz);
		break;
	case RSZ__ETALON:
		ResizeEtalon(input, output, scale);
	}
	return 0;
}

//************************************
// Method:    ResizeNearest
// FullName:  Resizer::ResizeNearest
// Access:    public 
// Returns:   int
// Qualifier: Fast an ugly
// Parameter: cv::Mat & input
// Parameter: cv::Mat & output
// Parameter: double scale
//************************************
int Resizer::ResizeNearest(cv::Mat & input, cv::Mat & output, double scale)
{
	if (scale == 1)// no need to resize
	{
		input.copyTo(output);
		return 0;
	}
	else {
		size_t new_size_x = cvRound((double)input.cols * scale);
		size_t new_size_y = cvRound((double)input.rows * scale);

		cv::Mat tmp = cv::Mat::zeros(new_size_y, new_size_x, input.type());

		for (size_t x = 0; x < new_size_x; ++x)
		{
			size_t nearest_x = cvRound((double)x / scale);

			for (size_t y = 0; y < new_size_y; ++y)
			{
				//std::cout << x << " - " << y << std::endl;
				size_t nearest_y = cvRound((double)y / scale);
				if (
					(nearest_x >= 0) &&
					(nearest_y >= 0) &&
					(nearest_x < input.cols) &&
					(nearest_y < input.rows)
					) {
					switch (input.type()) {
					case CV_8UC3:
						tmp.at<cv::Vec3b>(y, x) = input.at<cv::Vec3b>(nearest_y, nearest_x);
						break;
					case CV_8UC1:
						tmp.at<uchar>(y, x) = input.at<uchar>(nearest_y, nearest_x);
						break;
					}
				}
			}
		}
		tmp.copyTo(output);
	}
	return 0;
}

//************************************
// Method:    Approx_C1
// FullName:  Resizer::Approx_C1
// Access:    private 
// Returns:   void
// Qualifier:
// Parameter: double scale
// Parameter: cv::Mat & input
// Parameter: size_t nearest_y
// Parameter: size_t nearest_x
// Parameter: cv::Mat & tmp
// Parameter: size_t y
// Parameter: size_t x
// Parameter: double t_x
// Parameter: double t_y
//************************************
void Resizer::Approx_C1(
	double scale,
	cv::Mat &input, 
	size_t nearest_y, 
	size_t nearest_x, 
	cv::Mat &tmp, 
	size_t y, size_t x, 
	double t_x, 
	double t_y)
{
	uchar z0 = input.at<uchar>(nearest_y, nearest_x);

	int x1 = (int)((double)nearest_x - 1.); int y1 = (int)((double)nearest_y - 1.); uchar p1 = 0; x1 = x1 < 0 ? 0 : x1; y1 = y1 < 0 ? 0 : y1;
	if ((x1 >= 0) && (y1 >= 0) && (x1 < input.cols) && (y1 < input.rows))
		p1 = input.at<uchar>(y1, x1);
		
	int x2 = (int)((double)nearest_x - 1.); int y2 = (int)((double)nearest_y + 1.); uchar p2 = 0; x2 = x2 < 0 ? 0 : x2; y2 = y2 < 0 ? 0 : y2;
	if ((x2 >= 0) && (y2 >= 0) && (x2 < input.cols) && (y2 < input.rows))
		p2 = input.at<uchar>(y2, x2);
		
	int x3 = (int)((double)nearest_x + 1.); int y3 = (int)((double)nearest_y - 1.); uchar p3 = 0; x3 = x3 < 0 ? 0 : x3; y3 = y3 < 0 ? 0 : y3;
	if ((x3 >= 0) && (y3 >= 0) && (x3 < input.cols) && (y3 < input.rows))
		p3 = input.at<uchar>(y3, x3);
		
	int x4 = (int)((double)nearest_x + 1.); int y4 = (int)((double)nearest_y + 1.); uchar p4 = 0; x4 = x4 < 0 ? 0 : x4; y4 = y4 < 0 ? 0 : y4;
	if ((x4 >= 0) && (y3 >= 0) && (x4 < input.cols) && (y4 < input.rows))
		p4 = input.at<uchar>(y4, x4);

	double a0 = ( 1. * (double)p1 + 1. * (double)p2 + 1. * (double)p3 + 1. * (double)p4) / 4.;
	double a1 = (-1. * (double)p1 - 1. * (double)p2 + 1. * (double)p3 + 1. * (double)p4) / 4.;
	double a2 = (-1. * (double)p1 + 1. * (double)p2 - 1. * (double)p3 + 1. * (double)p4) / 4.;
	//double a3 = ( 1. * (double)p1 - 1. * (double)p2 - 1. * (double)p3 + 1. * (double)p4) / 4.;
	if ((x3 - x1) > 0 && (y2 - y1) > 0) {

		a0 = a0 - a1 * ((double)x3 + (double)x1) / ((double)x3 - (double)x1) - a2 * ((double)y2 + (double)y1) / ((double)y2 - (double)y1);
		a1 = 2 * a1 / ((double)x3 - (double)x1);
		a2 = 2 * a2 / ((double)y2 - (double)y1);

		double target_z = a0 + a1 * (t_x)+a2 * (t_y);

		if ((target_z < 255) && (target_z >= 0))
			tmp.at<uchar>(y, x) = cvRound(target_z);
	}else
	{
		tmp.at<uchar>(y, x) = z0;
	}
}

//************************************
// Method:    Approx_C3
// FullName:  Resizer::Approx_C3
// Access:    private 
// Returns:   void
// Qualifier:
// Parameter: double scale
// Parameter: cv::Mat & input
// Parameter: size_t nearest_y
// Parameter: size_t nearest_x
// Parameter: cv::Mat & tmp
// Parameter: size_t y
// Parameter: size_t x
// Parameter: double t_x
// Parameter: double t_y
//************************************
void Resizer::Approx_C3(
	double scale,
	cv::Mat &input, 
	size_t nearest_y, 
	size_t nearest_x, 
	cv::Mat &tmp, 
	size_t y, 
	size_t x, 
	double t_x, 
	double t_y)
{
	cv::Vec3b z0 = input.at<cv::Vec3b>(nearest_y, nearest_x);

	int x1 = (int)((double)nearest_x - 1.); int y1 = (int)((double)nearest_y - 1.); cv::Vec3b p1(0, 0, 0); x1 = x1 < 0 ? 0 : x1; y1 = y1 < 0 ? 0 : y1;
	if ((x1 >= 0) && (y1 >= 0) && (x1 < input.cols) && (y1 < input.rows))
		p1 = input.at<cv::Vec3b>(y1, x1);

	int x2 = (int)((double)nearest_x - 1.); int y2 = (int)((double)nearest_y + 1.); cv::Vec3b p2(0, 0, 0); x2 = x2 < 0 ? 0 : x2; y2 = y2 < 0 ? 0 : y2;
	if ((x2 >= 0) && (y2 >= 0) && (x2 < input.cols) && (y2 < input.rows))
		p2 = input.at<cv::Vec3b>(y2, x2);

	int x3 = (int)((double)nearest_x + 1.); int y3 = (int)((double)nearest_y - 1.); cv::Vec3b p3(0, 0, 0); x3 = x3 < 0 ? 0 : x3; y3 = y3 < 0 ? 0 : y3;
	if ((x3 >= 0) && (y3 >= 0) && (x3 < input.cols) && (y3 < input.rows))
		p3 = input.at<cv::Vec3b>(y3, x3);

	int x4 = (int)((double)nearest_x + 1.); int y4 = (int)((double)nearest_y + 1.); cv::Vec3b p4(0, 0, 0); x4 = x4 < 0 ? 0 : x4; y4 = y4 < 0 ? 0 : y4;
	if ((x4 >= 0) && (y3 >= 0) && (x4 < input.cols) && (y4 < input.rows))
		p4 = input.at<cv::Vec3b>(y4, x4);

	double a0b = ( 1. * (double)p1[0] + 1. * (double)p2[0] + 1. * (double)p3[0] + 1. * (double)p4[0]) / 4.;
	double a0g = ( 1. * (double)p1[1] + 1. * (double)p2[1] + 1. * (double)p3[1] + 1. * (double)p4[1]) / 4.;
	double a0r = ( 1. * (double)p1[2] + 1. * (double)p2[2] + 1. * (double)p3[2] + 1. * (double)p4[2]) / 4.;
	double a1b = (-1. * (double)p1[0] - 1. * (double)p2[0] + 1. * (double)p3[0] + 1. * (double)p4[0]) / 4.;
	double a1g = (-1. * (double)p1[1] - 1. * (double)p2[1] + 1. * (double)p3[1] + 1. * (double)p4[1]) / 4.;
	double a1r = (-1. * (double)p1[2] - 1. * (double)p2[2] + 1. * (double)p3[2] + 1. * (double)p4[2]) / 4.;
	double a2b = (-1. * (double)p1[0] + 1. * (double)p2[0] - 1. * (double)p3[0] + 1. * (double)p4[0]) / 4.;
	double a2g = (-1. * (double)p1[1] + 1. * (double)p2[1] - 1. * (double)p3[1] + 1. * (double)p4[1]) / 4.;
	double a2r = (-1. * (double)p1[2] + 1. * (double)p2[2] - 1. * (double)p3[2] + 1. * (double)p4[2]) / 4.;
	//double a3 = (1. * (double)p1 - 1. * (double)p2 - 1. * (double)p3 + 1. * (double)p4) / 4.;
	if ((x3 - x1) > 0 && (y2 - y1) > 0) {

		a0b = a0b - a1b * ((double)x3 + (double)x1) / ((double)x3 - (double)x1) - a2b * ((double)y2 + (double)y1) / ((double)y2 - (double)y1);
		a0g = a0g - a1g * ((double)x3 + (double)x1) / ((double)x3 - (double)x1) - a2g * ((double)y2 + (double)y1) / ((double)y2 - (double)y1);
		a0r = a0r - a1r * ((double)x3 + (double)x1) / ((double)x3 - (double)x1) - a2r * ((double)y2 + (double)y1) / ((double)y2 - (double)y1);
		a1b = 2 * a1b / ((double)x3 - (double)x1);
		a1g = 2 * a1g / ((double)x3 - (double)x1);
		a1r = 2 * a1r / ((double)x3 - (double)x1);
		a2b = 2 * a2b / ((double)y2 - (double)y1);
		a2g = 2 * a2g / ((double)y2 - (double)y1);
		a2r = 2 * a2r / ((double)y2 - (double)y1);

		double target_zb = a0b + a1b * (t_x)+a2b * (t_y);
		double target_zg = a0g + a1g * (t_x)+a2g * (t_y);
		double target_zr = a0r + a1r * (t_x)+a2r * (t_y);

		cv::Vec3b target_z(0, 0, 0 );
		if ((target_zb < 255) && (target_zb >= 0))
			target_z[0] = cvRound(target_zb);
		if ((target_zg < 255) && (target_zg >= 0))
			target_z[1] = cvRound(target_zg);
		if ((target_zr < 255) && (target_zr >= 0))
			target_z[2] = cvRound(target_zr);
		tmp.at<cv::Vec3b>(y, x) = target_z;
	}
	else
	{
		tmp.at<cv::Vec3b>(y, x) = z0;
	}
}

int Resizer::ResizeBilinear(cv::Mat& input, cv::Mat& output, double scale /*= 1*/, unsigned int w_sz /*= 4*/)
{
	if (scale == 1)// no need to resize
	{
		input.copyTo(output);
		return 0;
	}
	else {
		size_t new_size_x = cvRound((double)input.cols * scale);
		size_t new_size_y = cvRound((double)input.rows * scale);

		cv::Mat tmp = cv::Mat::zeros(new_size_y, new_size_x, input.type());

		for (size_t x = 0; x < new_size_x; ++x)
		{
			size_t nearest_x = cvRound((double)x / scale);
			double target_x = (double)x / scale;

			for (size_t y = 0; y < new_size_y; ++y)
			{
				size_t nearest_y = cvRound((double)y / scale);
				double target_y = (double)y / scale;
				if (
					(nearest_x >= 0) &&
					(nearest_y >= 0) &&
					(nearest_x < input.cols) &&
					(nearest_y < input.rows)
					) {
					switch (input.type()) {
					case CV_8UC3:
						Approx_C3(scale, input, nearest_y, nearest_x, tmp, y, x, target_x, target_y);
						break;
					case CV_8UC1:
						Approx_C1(scale, input, nearest_y, nearest_x, tmp, y, x, target_x, target_y);
						break;
					}
				}
			}

		}
		tmp.copyTo(output);
	}
	return 0;
}


//************************************
// Method:    ResizeEtalon
// FullName:  Resizer::ResizeEtalon
// Access:    public 
// Returns:   int
// Qualifier: Etalon from openCV
// Parameter: cv::Mat & input
// Parameter: cv::Mat & output
// Parameter: double scale
//************************************
int Resizer::ResizeEtalon(cv::Mat& input, cv::Mat& output, double scale /*= 1*/)
{
	try {
		cv::resize(input, output, output.size(), scale, scale, CV_INTER_LINEAR);
	}
	catch (...)
	{
		return -1;
	}
	return 0;
}


Resizer::~Resizer()
{
}
