#pragma once
#include <io.h>
#include <iostream>
#include <stdio.h>
#include "opencv\cv.hpp"
#include "opencv2\core.hpp"
#include "opencv\highgui.h"
#include "opencv\cv.h"
#include "opencv2\imgproc.hpp"
#include "Tools.h"

#define RSZ_NEAREST 0
#define RSZ_BILINER 1
#define RSZ__ETALON 2

class Resizer
{
public:
	Resizer();
	int ResizeImg(cv::Mat& input, cv::Mat& output, double scale = 1, unsigned int algorythm = RSZ_NEAREST, unsigned int w_sz = 4);
	int ResizeNearest(cv::Mat& input, cv::Mat& output, double scale = 1);
	int ResizeBilinear(cv::Mat& input, cv::Mat& output, double scale = 1, unsigned int w_sz = 4);
	int ResizeEtalon(cv::Mat& input, cv::Mat& output, double scale = 1);
	virtual ~Resizer();
private:
	void Approx_C1(double scale, cv::Mat &input, size_t nearest_y, size_t nearest_x, cv::Mat &tmp, size_t y, size_t x, double t_x, double t_y);
	void Approx_C3(double scale, cv::Mat &input, size_t nearest_y, size_t nearest_x, cv::Mat &tmp, size_t y, size_t x, double t_x, double t_y);
};

