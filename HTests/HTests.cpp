// HTests.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "Resizer.h"
#include "Segmenter.h"
#include "Tools.h"

int main()
{
	//////////////////////////////////////////////////////////////////////////
	//	Init
	//////////////////////////////////////////////////////////////////////////

	cv::Mat input;
	input = Tools::GetImage("C:\\work\\sideprojects\\HTests\\Tests\\Desert.jpg");

	//////////////////////////////////////////////////////////////////////////
	//	Resize
	//////////////////////////////////////////////////////////////////////////
	
	Resizer* r = new Resizer();

	cv::Mat o, oo, ooo;
	
	std::cout << "Linear resize" << std::endl;
	INIT_TIMECHECK();
	r->ResizeImg(input, o, 1 / sqrt(PI), RSZ_BILINER);
	STOP_TIMECHECK();

	std::cout << "NN resize" << std::endl;
	START_TIMECHECK();
	r->ResizeImg(input, oo, 1 / sqrt(PI), RSZ_NEAREST);
	STOP_TIMECHECK();

	std::cout << "OpenCV linear resize" << std::endl;
	START_TIMECHECK();
	r->ResizeImg(input, ooo, 1 / sqrt(PI), RSZ__ETALON);
	STOP_TIMECHECK();

	//////////////////////////////////////////////////////////////////////////
	//	Segment
	//////////////////////////////////////////////////////////////////////////

	Segmenter* s = new Segmenter();

	cv::Mat output;


	//////////////////////////////////////////////////////////////////////////
	// 
	// Segment image
	// Times passed in seconds : 0.020744 -> ~ 50 fps on intel i7
	//
	//////////////////////////////////////////////////////////////////////////
	std::cout << "Segment image" << std::endl;
	START_TIMECHECK();
	s->SegmentImg(input, output);
	STOP_TIMECHECK();

	cv::Mat sky, ground;
	s->ApplyMap(output, input, sky, ground);

	return 0;
	
}

