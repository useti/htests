#include "Segmenter.h"

Segmenter::Segmenter()
{
}

//************************************
// Method:    SegmentImg
// FullName:  Segmenter::SegmentImg
// Access:    public 
// Returns:   int
// Qualifier:
// Parameter: cv::Mat & input
// Parameter: cv::Mat & Output
//************************************
int Segmenter::SegmentImg(cv::Mat & input, cv::Mat & output)
{

	//////////////////////////////////////////////////////////////////////////
	// step 1 - downsize source image
	//////////////////////////////////////////////////////////////////////////

	cv::Mat downsampled = cv::Mat::zeros(DOWN_TO_ROWS, DOWN_TO_COLS, input.type());
	cv::resize(input, downsampled, downsampled.size(), 0, 0, CV_INTER_LANCZOS4);

//#ifdef _DEBUG
//	cv::imshow((char*)"downsampled", downsampled);
//	cv::waitKey(0);
//#endif

	//////////////////////////////////////////////////////////////////////////
	// step 2 - split channels, we a interested in b channel
	//////////////////////////////////////////////////////////////////////////
	std::vector<cv::Mat> chnls;
	cv::split(downsampled, chnls);

//#ifdef _DEBUG
//	cv::imshow((char*)"b", chnls[0]);
//	cv::imshow((char*)"g", chnls[1]);
//	cv::imshow((char*)"r", chnls[2]);
//	cv::waitKey(0);
//#endif

	//////////////////////////////////////////////////////////////////////////
	// step 3 - apply filters
	//////////////////////////////////////////////////////////////////////////

	cv::Mat kuwahara, blured;
	
	cv::blur(chnls[0], blured, cv::Size(GAUSS_KERNEL, GAUSS_KERNEL));

	cv::copyMakeBorder(blured, kuwahara, KUWAHARA_WIN, KUWAHARA_WIN, KUWAHARA_WIN, KUWAHARA_WIN, cv::BORDER_CONSTANT, cv::Scalar(0));

	kuwahara = Tools::KuwaharaGray(kuwahara, KUWAHARA_WIN);

//#ifdef _DEBUG
//	cv::imshow((char*)"Gaussian blur", blured);
//	cv::imshow((char*)"Kuwahara", kuwahara);
//	cv::waitKey(0);
//#endif

	//////////////////////////////////////////////////////////////////////////
	//	step 4 - thresold
	//////////////////////////////////////////////////////////////////////////

	double mn;
	double mx;

	cv::minMaxIdx(kuwahara, &mn, &mx);

	double mid = (mx - mn) / 2.;

	cv::Mat mask(
		kuwahara,
		cv::Rect(KUWAHARA_WIN, KUWAHARA_WIN, kuwahara.cols - 2 * KUWAHARA_WIN, kuwahara.rows - 2 * KUWAHARA_WIN));

	for (size_t rw_idx = 0; rw_idx < mask.rows; ++rw_idx)
	{
		cv::Mat row = mask.row(rw_idx);
		for (size_t cl_idx = 0; cl_idx < row.cols; ++cl_idx) 
		{
			row.at<uchar>(cl_idx) = row.at<uchar>(cl_idx) > mid ? 255 : 0;
		}
	}


//#ifdef _DEBUG
//	cv::imshow((char*)"thresolded", mask);
//	cv::waitKey(0);
//#endif

	//////////////////////////////////////////////////////////////////////////
	// step 5 - upscale mask to original image size
	//////////////////////////////////////////////////////////////////////////

	cv::resize(mask, output, input.size(), 0, 0, CV_INTER_CUBIC);

//#ifdef _DEBUG
//	cv::imshow((char*)"output", output);
//	cv::waitKey(0);
//#endif

	return 0;
}

int Segmenter::ApplyMap(cv::Mat & map, cv::Mat & input, cv::Mat & sky, cv::Mat & ground)
{
	sky = cv::Mat::zeros(input.size(), input.type());
	ground = cv::Mat::zeros(input.size(), input.type());


	for (size_t row_idx = 0; row_idx < map.rows; ++row_idx)
	{
		for (size_t col_idx = 0; col_idx < map.cols; ++col_idx)
		{
			//std::cout << col_idx << " - " << row_idx << std::endl;
			if (map.at<uchar>(row_idx, col_idx)> 0)
			{
				switch (input.type()) {
				case CV_8UC3:
					sky.at<cv::Vec3b>(row_idx, col_idx) = input.at<cv::Vec3b>(row_idx, col_idx);
					break;
				case CV_8UC1:
					sky.at<uchar>(row_idx, col_idx) = input.at<uchar>(row_idx, col_idx);
					break;
				}
			}
			else
			{
				switch (input.type()) {
				case CV_8UC3:
					ground.at<cv::Vec3b>(row_idx, col_idx) = input.at<cv::Vec3b>(row_idx, col_idx);
					break;
				case CV_8UC1:
					ground.at<uchar>(row_idx, col_idx) = input.at<uchar>(row_idx, col_idx);
					break;
				}
			}
		}
	}
#ifdef _DEBUG
	cv::imshow((char*)"input", input);
	cv::imshow((char*)"sky", sky);
	cv::imshow((char*)"ground", ground);
	cv::waitKey(0);
#endif
	return 0;
}


Segmenter::~Segmenter()
{
}
