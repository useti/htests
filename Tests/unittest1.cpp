#include "stdafx.h"
#include "CppUnitTest.h"
#include "Resizer.cpp"
#include "Segmenter.cpp"
#include "Tools.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{		
	TEST_CLASS(Tests4H)
	{
	public:
		
		TEST_METHOD(SegmenterInit)
		{ 
			Segmenter* s = new Segmenter();
			Assert::IsNotNull(s);
		}

		TEST_METHOD(ResizerInit)
		{
			Resizer* r = new Resizer();
			Assert::IsNotNull(r);
		}

		TEST_METHOD(ToolsInit) 
		{
			Tools* t = new Tools();
			Assert::IsNotNull(t);
		}

		TEST_METHOD(Segmenter_SegmentZero)
		{
			Segmenter* s = new Segmenter();
			Assert::IsNotNull(s);
			cv::Mat input = cv::Mat::zeros(cv::Size(1280, 1024), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output = cv::Mat::zeros(cv::Size(1280, 1024), CV_8UC3);
			s->SegmentImg(input, output);
		}

		TEST_METHOD(Resizer_ResizeZero)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			r->ResizeImg(input, output);
		}

		TEST_METHOD(Resizer_ResizeNearestUpscale)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output;
			r->ResizeImg(input, output, sqrt(PI), RSZ_NEAREST);
		}

		TEST_METHOD(Resizer_ResizeNearestDownscale)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output;
			r->ResizeImg(input, output, 1/ sqrt(PI), RSZ_NEAREST);
		}

		TEST_METHOD(Resizer_ResizeLinearUpscale)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output;
			r->ResizeImg(input, output, sqrt(PI), RSZ_BILINER);
		}

		TEST_METHOD(Resizer_ResizeLinearDownscale)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output;
			r->ResizeImg(input, output, 1 / sqrt(PI), RSZ_BILINER);
		}

		TEST_METHOD(Resizer_ResizeEtalontUpscale)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output;
			r->ResizeImg(input, output, sqrt(PI), RSZ__ETALON);
		}

		TEST_METHOD(Resizer_ResizeEtalonDownscale)
		{
			Resizer* r = new Resizer();
			cv::Mat input = cv::Mat::zeros(cv::Size(640, 480), CV_8UC3);
			input = cv::Scalar(255, 0, 255);
			cv::Mat output;
			r->ResizeImg(input, output, 1/ sqrt(PI), RSZ__ETALON);
		}

		TEST_METHOD(Segmenter_SegmentImage)
		{
			Segmenter* s = new Segmenter();
			cv::Mat input = Tools::GetImage("C:\\work\\sideprojects\\HTests\\Tests\\Desert.jpg");
			cv::Mat output;
			s->SegmentImg(input, output);
			Assert::AreEqual(input.rows, output.rows);
			Assert::AreEqual(input.cols, output.cols);
		}
	};
}